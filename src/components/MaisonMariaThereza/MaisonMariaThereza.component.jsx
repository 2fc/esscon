import React from 'react'

import * as Styled from './MaisonMariaThereza.styles';

const MaisonMariaThereza = () => {

  return(
    <Styled.MaisonMariaThereza>
      
      <div className='header'>Maison Maria Thereza</div>
      
      <div className='hero'>
        <img src="./images/empreendimentos/mmt/logoMMT.png" alt="" />
      </div>

      <div className='inner-wrapper'>
        <div className='descricao'>Nobre e arrojado, de alto nível, com elegância e sofisticação, qualidade e custo benefício, para a arte de viver bem.</div>
      </div>

      <div className='inner-wrapper'>
        <img src="./images/empreendimentos/mmt/bemvindo.png" alt="" />
        <div className='subTitulo'>
          <div className='titulo'>Seja Bemvindo</div>
          Localizado na tranquila Rua Guaicurus, numa altitude de 1.230m, no topo da colina, proporcionando usufruir de vista livre. A beleza única e natural das montanhas, da serra e da cidade, beijado pelo sol.
        </div>
      </div>

      <div className='inner-wrapper'>
        <div className='titulo'>Um residencial cheio de detalhes.</div>
      </div>

      <div className='inner-wrapper'>
        <div className='titulo'>Um marco na história de Poços de Caldas</div>
        <img src="./images/empreendimentos/mmt/detalhes1.png" alt="" />
      </div>

      <div className='inner-wrapper'>
        <img src="./images/empreendimentos/mmt/detalhes2.png" alt="" />
        <div className='subTitulo'>
          "MAISON MARIA THEREZA traz a força da natureza, aliada à uma arquitetura de excelência arrojada e moderna, com notas de arte, com cada detalhe compondo e trazendo harmonia, equilíbrio e bem estar.""
        </div>
      </div>


      <div className='inner-wrapper'>
        <div className='subTitulo'>
          <div className='titulo'>Você procura algo exclusivo?</div>
            Viva a experiência de viver com qualidade de vida, com estilo e exclusividade. 
            Um projeto assinado, único e especial, inclusive de interiores.
        </div>
        <img src="./images/empreendimentos/mmt/bemvindo1.png" alt="" />
      </div>

      <div className='inner-wrapper'>
        <div className='titulo'>Um residencial incomparável, único e especial.</div>
      </div>

      <div className='inner-wrapper'>
        
        <div className='subTitulo'>
        <div className='titulo'>Diferenciais exclusivos</div>
          Viva a experiência de viver com qualidade de vida, com estilo e exclusividade. 
          Um projeto assinado, único e especial, inclusive de interiores.
        </div>
        <img src="./images/empreendimentos/mmt/diferenciais1.png" alt="" />
      </div>

      <div className='inner-wrapper'>
        <img src="./images/empreendimentos/mmt/diferenciais2.png" alt="" />
        <div className='subTitulo'>
          <ul>
            <li>Roof Top copleto com mirante</li>
            <li>Pele de vidros bronze refletivos</li>
            <li>Hydros Jacuzzi com água quente</li>
            <li>Todas as suítes com varandas</li>
            <li>Varanda gourmet com churrasqueira Portas balcão e persianas automatizadas Medidores individuais de água e luz Elevador de última geração</li>
            <li>Estação de recarga para veículos elétricos Torre priorizando vista e posição solar Tomadas USB nos dormitórios</li>
          </ul>
        </div>
      </div>

      <div className='inner-wrapper'>
        <div className='titulo'>Tipologia</div>
      </div>
      
      <div className='inner-wrapper'>
        <img src="./images/empreendimentos/mmt/tipologia.png" alt="" />
        <div className='subTitulo'>Planta baixa, pavimento tipo 1, 2, 3 e 4
          <br/><br/>Planta baixa térreo
        </div>  
      </div>


      <div className='inner-wrapper'>
        <div className='subTitulo'>Varanda Gourmet</div>  
        <img src="./images/empreendimentos/mmt/varanda_gourmet.png" alt="" />
      </div>

      <div className='inner-wrapper'>
        <img src="./images/empreendimentos/mmt/cozinha_varanda.png" alt="" />
        <div className='subTitulo'>Cozinha com Varanda</div>  
      </div>

      <div className='inner-wrapper'>
        <div className='subTitulo'>Ilha Gourmet</div>  
        <img src="./images/empreendimentos/mmt/ilha_gourmet.png" alt="" />
      </div>

      <div className='inner-wrapper'>
        <img src="./images/empreendimentos/mmt/suite_varanda.png" alt="" />
        <div className='subTitulo'>Suíte com Varanda</div>  
      </div>

      <div className='inner-wrapper'>
        <div className='subTitulo'>Cozinha com ilha gourmet</div>
        <img src="./images/empreendimentos/mmt/cozinha_gourmet.png" alt="" />
      </div>

      <div className='inner-wrapper'>
        <img src="./images/empreendimentos/mmt/living_integrado.png" alt="" />
        <div className='subTitulo'>Living integrado</div>
      </div>



      <div className='inner-wrapper'>
        <div className='titulo'>Área de lazer na cobertura</div>
      </div>
    
      <div className='inner-wrapper'>
        <div className='subTitulo'>Pavimento<br/><br/>Planta baixa subsolo</div>
        <img src="./images/empreendimentos/mmt/lazer_cobertura.png" alt="" />
      </div>

      <div className='inner-wrapper'>
        <img src="./images/empreendimentos/mmt/cob_coz_integ_gourmet.png" alt="" />
        <div className='subTitulo'>Cozinha integrada com ilha gourmet</div>  
      </div>

      <div className='inner-wrapper'>
        <div className='subTitulo'>Suíte com varanda</div>
        <img src="./images/empreendimentos/mmt/cob_suite_varanda.png" alt="" />
      </div>

      <div className='inner-wrapper'>
        <img src="./images/empreendimentos/mmt/cob_living_integrado.png" alt="" />
        <div className='subTitulo'>Living integrado</div>  
      </div>
    
      <div className='inner-wrapper'>
        <div className='subTitulo'>Moderna e sofisticada, com muito conforto. Viva a experiência de viver com qualidade de de vida.
        <br/><i>Viver a vida em toda a sua plenitude, esse é o melhor projeto.</i></div>
        <img src="./images/empreendimentos/mmt/lazer_cobertura1.png" alt="" />
      </div>

      <div className='inner-wrapper'>
        <img src="./images/empreendimentos/mmt/lazer_cobertura2.png" alt="" />
        <div className='subTitulo'>Hidro</div>  
      </div>

      <div className='inner-wrapper'>
        <div className='subTitulo'>Vista superior</div>
        <img src="./images/empreendimentos/mmt/lazer_vista_sup.png" alt="" />
      </div>

      <div className='inner-wrapper'>
        <img src="./images/empreendimentos/mmt/lazer_planta_sup.png" alt="" />
        <div className='subTitulo'>ROOF TOP</div>
      </div>




      <div className='inner-wrapper'>
        <div className='titulo'>Área de lazer</div>
      </div>

      <div className='inner-wrapper'>
        <div className='subTitulo'>Espaço kids</div>
        <img src="./images/empreendimentos/mmt/lazer_espaco_kids.png" alt="" />
      </div>

      <div className='inner-wrapper'>
        <img src="./images/empreendimentos/mmt/lazer_espaco_kids1.png" alt="" />
        <div className='subTitulo'>Espaço kids</div>
      </div>
      

      <div className='inner-wrapper'>
        <div className='subTitulo'>Espaço pet</div>
        <img src="./images/empreendimentos/mmt/lazer_espaco_pets.png" alt="" />
      </div>

      <div className='inner-wrapper'>
        <img src="./images/empreendimentos/mmt/lazer_hidro.png" alt="" />
        <div className='subTitulo'>Hidro SPA</div>
      </div>

      <div className='inner-wrapper'>
        <div className='subTitulo'>Academia</div>
        <img src="./images/empreendimentos/mmt/lazer_academia.png" alt="" />
      </div>

      <div className='inner-wrapper'>
        <img src="./images/empreendimentos/mmt/lazer_salao_festa.png" alt="" />
        <div className='subTitulo'>Salão de Festa</div>
      </div>




      <div className='inner-wrapper'>
        <div className='titulo'>Sobre a construtora e incorporadora</div>
      </div>

      <div className='inner-wrapper'>
        <img src="./images/empreendimentos/mmt/mTereza.png" alt="" />
        <div className='subTitulo'>
          <div className='titulo'>MAISON MARIA THEREZA</div>
          A escolha do nome não poderia ser melhor!
          Uma mulher de estilo clássico, mas muito a frente do seu tempo. Nascida em nossa linda cidade, nos propôs um estilo de vida onde o simples era chique e arrojado.
          Em sua beleza Matriarcal nos encinos virtudes que marcam nossas vidas até hoje.
          É isso que queremos refletir nesta bela obra.
          A obra de uma vida!
        </div>
      </div>

      <div className='inner-wrapper'>
        <div className='subTitulo'>
          <div className='titulo'>ASHLEY DO CARMO</div>
          ...
        </div>
        <img src="./images/empreendimentos/mmt/ashleyCarmo.png" alt="" />
      </div>



      <div className='inner-wrapper'>
        <img src="./images/empreendimentos/mmt/arquit.png" alt="" />
        <img src="./images/empreendimentos/mmt/engenharia.png" alt="" />
      </div>

    </Styled.MaisonMariaThereza>
  )

}

export default MaisonMariaThereza;