import React from 'react'
import * as Styled from './Card.styles';
import Button from '../Button';

function Card({ maisonMariaThereza, miami }) {
  return (
    <Styled.Card>
      <div className='inner-wrapper'>
          <div className='imgBox'>
            <img className='imgBox__image' src='./images/empreendimentos/mmt/empreend_MMT.png' alt='' />
          </div>
        
          <div className='content'>
            <div className='content__tittle'>MAISON MARIA THEREZA</div>
            <div className='content__description'>Alto padrão</div>
            <div className='content__local'>Poços de Caldas - MG</div>
            <Button onAction={ maisonMariaThereza }>Saiba mais</Button>
          </div>
      </div>
      

    </Styled.Card>
  )
}

export default Card;
