import styled from "styled-components";

export const Sobre = styled.div`
  left: 0;
  margin: 0px;
  right: 0;
  padding: 0;
  width: 100vw;

  @media (max-width: 768px) {
    height: auto;
  }

  .inner-wrapper {  
    align-items: center;
    color: #111;
    display: flex;
    justify-content: space-around;
    margin: 30px 0;
    padding: 30px 0;

    p {
      font-size: 14px;
    }

    @media (max-width: 768px) {
      flex-direction: column;
    }
  }

  .header {
    align-items: center;
    color: rgba(22, 74, 115, 1); 
    font-family: 'Cinzel';
    font-size: 36px;
    font-weight: 600;
    margin-top: 30px;
    padding: 18px 0;
    padding-left: 10%;
    position: relative;
    text-shadow: 1px 3px 3px #ccc;
    width: 100vw;

    @media (max-width: 768px) {
      margin-top: 15px;
      padding-left: 5%;
    }
  }


  .box {
    align-items: center;
    color: rgba(22, 74, 115, 1);
    display: flex;
    flex-direction: column;
    justify-content: center;
    padding: 30px;
    text-align: center;
    width: 81%;

    img{
      display: flex;
      flex-direction: column;
    }

    h4{
      line-height: 5;
    }

    p{
      line-height: 2.1;
      text-align: left;
    }
  }

`;
