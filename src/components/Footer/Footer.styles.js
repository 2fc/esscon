import styled from "styled-components";

export const Footer = styled.div`
  background: radial-gradient(rgba(22, 74, 115, 1), rgba(22, 74, 115, .9)); // Azul Royal #164A73
  bottom: 0;
  display: flex;
  height: auto;
  justify-content: space-around;
  left: 0;
  margin: 0;
  right: 0;
  text-align: center;
  padding: 45px 9px;
  width: 100vw;

  @media (max-width: 768px) {
    flex-direction: column;
    height: auto;
  }

  .inner-wrapper {
    color: #ccc;
    font-size: 1.2rem;
    margin: 15px 21px;
    width: 100vw;

    h4 {
      border-bottom: 1px solid #ccc;
      margin-bottom: 9px;
      padding: 6px 9px; 
    }

  }

  .navbar-LinkContainer {
    display: flex;
    flex-direction: column;
    font-size: 0.9rem;

    p {
      font-size: 0.9rem;
      text-align: left;
      text-decoration: none;
      font-family: Arial, Helvetica, sans-serif;
      padding-left: 21px;
      padding-top: 9px; 
    }
  }

  .navbar-Link {
    color: #ccc;
    font-family: Arial, Helvetica, sans-serif;
    text-align: left;
    padding: 12px 0px 9px 21px;

    a {
        color: #ccc;
        text-decoration: none;
        // font-family: 'Caros Soft Regular';
        font-size: '14px';
        line-height: 17px;
        margin-bottom: 16px;
        min-width: 170px;

        @media(max-width: 768) {
          margin-bottom: 7px;
        }

        &:last-child {
          margin-bottom: 0;
        }
        
        &:hover {
          //font-family: 'Caros Soft Bold';
        }
      }

  }

  .nav-item {
    padding-bottom: 18px;
  }

  .nav-link {
    color: #ccc;

    &:hover {
      border-bottom: 3px solid #ccc;
    }
  }

  .social-media {
    &__icon {
      align-items: center;
      background-color: rgba(255, 255, 255, 0.9);
      background-position: center;
      background-repeat: no-repeat;
      border-radius: 50%;
      // display: inline-flex; 
      height: 39px;
      margin-left: 15px;
      justify-content: center;
      overflow: hidden;
      width: 39px;
      transition: background-color .15s ease-in-out;
      text-indent: -1000px;

      @media(min-width: 768) {
        margin-left: initial;
        margin-right: 16px;
      }

      &:hover {
        background-color: rgba(255, 2555, 255, 0.3);
      }

      &--whatsapp {
        background-image: url(/images/icons/logo-whatsapp.svg);
        background-position: 4px 4px;
        background-size: 81%;
      }
    }
  }

`;
