import styled from "styled-components";

export const Contato = styled.div`
  left: 0;
  margin: 0px;
  right: 0;
  padding: 0;
  width: 100vw;

  @media (max-width: 768px) {
    height: auto;
  }

  .inner-wrapper {  
    align-items: center;
    color: #111;
    display: flex;
    justify-content: space-around;
    margin: 30px 0;
    padding: 30px 0;

    p {
      font-size: 14px;
    }

    @media (max-width: 768px) {
      flex-direction: column;
    }
  }

  .header {
    align-items: center;
    color: rgba(22, 74, 115, 1); 
    font-family: 'Cinzel';
    font-size: 36px;
    font-weight: 600;
    margin: 30px;
    padding: 18px 0;
    padding-left: 10%;
    position: relative;
    text-shadow: 1px 3px 3px #ccc;
    width: 100vw;

    @media (max-width: 768px) {
      margin-top: 15px;
      padding-left: 5%;
    }
  }

  .map {
    border-radius: 6px;
    box-shadow: 0px 5px 20px rgba(0, 0, 0, 0.5);
    background-image: url(/images/map.png);
    height: 700px;
    margin: 15px;
    width: 450px;

    h2 {
      background-color: rgba(255, 255, 255, 0.9);
      border-bottom: 6px solid rgba(22, 74, 115, 1);
      color: rgba(22, 74, 115, 1);
      margin: 54% 15%;
      padding: 15px;
      text-align: center;
    }
  }

  .fones {
    align-items: center;
    border-radius: 6px;
    box-shadow: 0px 5px 20px rgba(0, 0, 0, 0.5);
    color: rgba(22, 74, 115, 1);
    display: flex;
    flex-direction: column;
    justify-content: center;
    height: 700px;
    margin: 15px;
    padding: 30px;
    text-align: center;
    width: 450px;

    h4{
      line-height: 5;
    }

    h3{
      line-height: 6;
    }

    p{
      line-height: 3;
    }
  }

`;
