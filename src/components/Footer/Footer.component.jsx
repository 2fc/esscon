import React, { useState, useEffect } from 'react';
import { getTopNav } from '../Data/MainNav.data';
import * as Styled from './Footer.styles'

// import { 
//   Container,
//   InnerContainer,
//   NavbarLinkContainer,
//   NavbarLink } from './Footer.styles';

function Footer() {
  
  const [navItems, setNavItems] = useState([]);
  useEffect(() => {
    setNavItems(getTopNav());
  }, [])


  return(
    <Styled.Footer>
        <div className='inner-wrapper'>
          <h4>Contato</h4>
          <div className='navbar-LinkContainer'>
            <p>Escritório:</p>
            <div className='navbar-Link'>
              <a 
                href="https://api.whatsapp.com/send?phone=5535999872627&text=Olá,%20Tenho%20interesse%20em%20saber%20mais%20sobre%20a%20empresa,%20obrigado!" 
                target="_blank"
                rel="noopener noreferrer"
              >
                (35) 9 9987-2627
              </a>
            </div>
            
            <br/>
            <p>Vendas:</p>
            <div className='navbar-Link'>
              <a 
                href="https://api.whatsapp.com/send?phone=5535998893840&text=Olá,%20Tenho%20interesse%20em%20saber%20mais%20sobre%20os%20empreendimentos,%20obrigado!" 
                target="_blank"
                rel="noopener noreferrer"
              >
                (35) 3722-3840 <br/><br/>(35) 9 9889-3840
              </a> 
            </div>

            </div>
        </div>

        <div className='inner-wrapper'>
          <h4>Localização</h4>
          <div className='navbar-LinkContainer'>
            <div className='navbar-Link' to=''>Praça pedro Sanches, 145 - Centro<br/></div>
          </div>
        </div>

        <div className='inner-wrapper'>
          <h4>Empreendimentos</h4>
          <div className='navbar-LinkContainer'>
            <div className='navbar-Link' to='/'>Maison Maria Teresa</div>
          </div>
        </div>

        <div className='inner-wrapper'>
          <h4>Menu</h4>
          <div className='navbar-LinkContainer'>
            <div className='navbar-Link'>
              <ul>
                {navItems.map((item) => (
                <li key={item.id} className='nav-item'>
                  <a href={item.href} className='nav-link'>{item.label}</a>
                </li>
                ))}
              </ul>

            </div>
          </div>

        </div>
      
    </Styled.Footer>
  )
}

export default Footer;

