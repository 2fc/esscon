import React, { useState, useEffect } from 'react';
import { getTopNav } from '../Data/MainNav.data';
import Button from '../Button';
import * as Styled from './MainNav.styles';

function MainNav({vendas}) {

  const [navItems, setNavItems] = useState([]);
  useEffect(() => {
    setNavItems(getTopNav());
  }, [])

  const [active, setActive] = useState('nav__active');
  const [toggleIcon, setToggleIcon] = useState('nav__toggler');

  const navToggle = () => {
    active === 'nav__active' 
      ? setActive('nav__menu') 
      : setActive('nav__active') 

    toggleIcon === 'nav__toggler' 
      ? setToggleIcon('nav__toggler toggle') 
      : setToggleIcon('nav__toggler')
  }

  return (
    <Styled.MainNav>
      <div className='inner-wrapper'>

        <div onClick={navToggle} className={toggleIcon}>
          <div className='line1'></div>
          <div className='line2'></div>
          <div className='line3'></div>
        </div>

        <ul className={active}>
          {navItems.map((item) => (
            <li key={item.id} className='nav__item'>
              <a href={item.href} className='nav__link'>{item.label}</a>
            </li>
            ))}
        </ul>


        <Styled.Logo alt="Essencial - Construtora e Incorporadora"/>

        <div className='nav__contact'>
          <Button onAction={vendas}>(35) 9 9889-3840</Button>
        </div>

      </div>
    </Styled.MainNav>
  );
}

export default MainNav;
