import styled from "styled-components";

export const Card = styled.div` // .container
  align-items: center;
  border: 0px solid rgba(22, 74, 115, 1);
  display: flex;
  flex-wrap: wrap;
  font-family: 'Lato', Arial, Helvetica, sans-serif;
  min-height: 570px;
  justify-content: space-around;
  padding: 30px;
  position: relative;
  width: 100vw;

  .inner-wrapper {
    background: rgba(22, 74, 115, .9);
    border-radius: 6px;
    box-shadow: 0px 5px 20px rgba(0, 0, 0, 0.5);
    display: flex;
    flex-direction: column;
    height: 260px;
    margin: 30px 10px;
    padding: 20px 15px;
    position: relative;
    transition: 0.3s ease-in-out;
    max-width: 300px;

    &:hover {
      height: 420px;
    
      .content{
        margin-top: -60px;
        opacity: 1;
        transition-delay: 0.3s;
        visibility: visible;
      }
    }
  }

  .imgBox {
    align-items: center;
    box-shadow: 0px 5px 20px rgba(0, 0, 0, 0.2);
    height: 260px;
    
    position: relative;
    top: -60px;
    width: 260px;
    z-index: 1;
    
    &__image {
      border-radius: 6px;
      max-width: 100%;
    }

  }

  .content {
    border: 0px solid rgba(22, 74, 115, 1);
    color: #eee;
    margin-top: -120px;
    opacity: 0;
    padding: 3px 0;
    position: relative;
    text-align: center;
    transition: 0.3s ease-in-out;
    visibility: hidden;

    &__tittle {
      padding-top: 9px;
      padding-bottom: 3px;
    }

    &__description {
      font-size: small;
      padding: 6px 0px;
    }

    &__local {
      font-size: small;
      padding-top: 6px 0;
      padding-bottom: 21px;
    }
  }

`;

