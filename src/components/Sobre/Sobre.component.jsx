import React from 'react'
import * as Styled from '../Sobre/Sobre.styles';

const Sobre = () => {
  return (
    <Styled.Sobre>
      <div className='header'>Sobre</div>
      <div className='inner-wrapper'>

        <div className='box'>
          <h4>Nossa história começa em ...</h4>
          
          <div className='box'>
            <p>A Essencial Comstrutora inicias suas atividades a  partir de ..., com o propósito...</p>
          </div>
        
        </div>

      </div>
    </Styled.Sobre>
  )
}

export default Sobre
