import styled from "styled-components";

export const MaisonMariaThereza = styled.div`
  /* background: linear-gradient(rgba(122, 130, 122, 1), rgba(232, 232, 232, 0.6)); */
  background-color: #fff;
  bottom: 0;
  display:flex;
  flex-direction: column;
  height: auto;
  justify-content: space-around;
  left: 0;
  margin: 0;
  right: 0;
  padding: 12px;
  text-align: center;
  width: 100%;

  @media (min-width: 700px) {
    height: auto;
  }

  .header {
    align-items: center;
    color: rgba(22, 74, 115, 1); 
    font-family: 'Cinzel';
    font-size: 36px;
    font-weight: 600;
    margin-top: 30px;
    padding: 18px 0;
    padding-left: 10%;
    position: relative;
    text-shadow: 1px 3px 3px #ccc;
    width: 100vw;

    @media (max-width: 768px) {
      margin-top: 15px;
      padding-left: 5%;
    }
  }


  .inner-wrapper {
    align-items: center;
    color: #111;
    display: flex;
    justify-content: space-around;
    margin: 21px;
    width: 100%;
    padding: 45px 30px;

    p {
      font-size: 14px;
    }
  }

  .navbarLink {
    color: #111;
    font-size: 14px;
    font-family: Arial, Helvetica, sans-serif;
    padding: 3px 9px;
  }

  .hero {
    align-items: center;
    display: flex;
    justify-content: center;
    margin: 30px 90px;
    padding: 10px;

    @media (max-width: 700px) {
      flex-direction: column ;
    }
  }

  .titulo {
    font-size: 2rem;
    text-shadow: 1px 3px 3px #ccc;
    padding: 15px 30px 30px 15px;
  }

  .subTitulo {
    font-size: 1.5rem;
    padding: 45px;
    text-align: left;
    text-shadow: 1px 3px 3px #ccc;
  }

  .descricao {
    align-items: center;
    font-size: 2rem;
    text-shadow: 1px 3px 3px #ccc;
    padding: 15px;
  }
`;
