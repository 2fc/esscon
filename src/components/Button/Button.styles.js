import styled from 'styled-components';

export const Button = styled.button`
  background-color: rgba(22, 74, 115, .9);
  border: none;
  border-radius: 12px;
  color: rgba(255, 255, 255, 1);
  cursor: pointer;
  font-family: 'Cinzel';
  font-size: 14px;
  font-weight: 600;
  padding: 12px;
  text-transform: uppercase;

  @media (max-width: 768px) {
    padding: 12px;
  }

  &:hover {
    background-color: rgba(255, 255, 255, 1);
    color: rgba(22, 74, 115, .9);
  }
`;