import React from 'react';
import styled from 'styled-components';
import { useMediaQuery } from 'react-responsive';

import LogoImage from '../../assets/essencial_logo.png';
import LogoImageShort from '../../assets/essencial_logo_icon.png';

export const MainNav = styled.div`
  /* background-color: transparent; */
  background: linear-gradient(to top, rgba(22, 74, 115, .01), rgba(255, 255, 255, 1));
  text-align: center;
  position: relative;
  width: 100vw;
  z-index: 1;

  .inner-wrapper{
    align-items: center;
    display: flex;
    height: 12vh;
    justify-content: space-between;
    padding: 0 15px;

    @media(max-width: 768px) {
      padding: 6px;
    }
  }

  .nav__brand {
    align-items: center;
    color: #111;
    font-family: 'Lato', Arial, Helvetica, sans-serif;
    font-size: 30px;
    font-weight: 600;
    text-align: center;

    h6{
      font-size: 12px;
    }
  }

  .nav__menu {
    align-items: center;
    display: flex;
    justify-content: space-around;
    gap: 3rem;
  }

  .nav__toggler div {
    background: rgba(22, 74, 115, 1);
    height: 0.2rem;
    margin: 0.4rem;
    transition: 0.21s ease-in;
    width: 3rem;
  }

  .nav__toggle {
    cursor: pointer;
    display: block;
  }

  .nav__active {
    display: none;
    transform: translateX(0%);
    transition: .3s ease-in;
  }

  /* Toggler Icon Animation */
  .toggle .line1 {
    transform: translate(-5px, 7px) rotate(-45deg);
  }
  .toggle .line2 {
    opacity: 0;
  }
  .toggle .line3 {
    transform: translate(-5px, -7px) rotate(45deg);
  }


  .nav__menu {
    align-items: center;
    background: linear-gradient(to right, rgba(255, 255, 255, .9), rgba(255, 255, 255, .9));
    color: #164A73;
    display: flex;
    flex-direction: column;
    justify-content: space-around;
    height: 89vh;
    left: 0;
    position: absolute;
    top: 135px;
    transform: translateX(0%);
    transition: 0.3s ease-in;
    width: 100%;
    z-index: 1;
    
  }

  .nav__item {
    border-bottom: 1px solid #164A73; // rgba(227, 189, 20, 1);
    color: #164A73;
    display: inline-block;
    font-weight: bold;
    padding: 12px;
    text-transform: uppercase;
    width: 90vw;

  }

  .nav__link {
    background-color: rgba(255, 255, 255, .01);
    color: #164A73; // rgba(227, 189, 20, 1); Dourado
    width: 90vw;
    padding: 12px 30px;


    &:hover {
      background-color: #164A73;
      border-radius: 9px;
      color: rgba(255, 255, 255, 1);
    }
  }

  .nav__contact {
    color: #eee;
    display: block;
    text-align: center;

    p {
      background: radial-gradient(rgba(255, 255, 255, .6), rgba(255, 255, 255, .9)); // Azul Royal #164A73 */
      border-radius: 9px;
      color: rgba(22, 74, 115, 1);
      font-size: 12px;
      margin: 0;
      padding: 6px;
    }

  }

`;



export const Logo = styled((props) => {

  const isMobile = useMediaQuery({ maxWidth: 768});
  const logo = !isMobile ? LogoImage : LogoImageShort;
  return <img src={logo} {...props} alt='Essencial - Construtora e Incorporadora' />
})`
  height: 177px;
  width: 210px;
`;
